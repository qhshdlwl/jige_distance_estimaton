### config.py - 카메라 스펙, 사람 키 등의 prerequisites
### models.py - 클래스 정보(카메라, 객체, 탐지된 객체 등등)
### estimations.py - 수학적 모델링 및 계산 함수
### main.py - 메인 로직(input: YOLO detect 결과 // output: 객체간 거리)
