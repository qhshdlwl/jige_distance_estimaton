# Units - length[m], angle[degree]

# ========= Camera Configuration =========

FOCAL_LENGTH = 0.004

RES_X = 1920
RES_Y = 1080

VFOV = 79
HFOV = 43

CAMERA_HEIGHT = 2.5
TILT_ANGLE = 10

# ========= Default Object Size =========

HEIGHT_HUMAN = 1.7
HEIGHT_FORK = 2.3
WIDTH_HUMAN = 0.4
WIDTH_FORK = 2

# ========= YOLOv5 Configuration =========


YOLO_MODEL_PATH = "path/to/yolov5/model.weights"
YOLO_CONFIG_PATH = "path/to/yolov5/config.cfg"
CONFIDENCE_THRESHOLD = 0.5

# ========= Visualization Configuration =========


SHOW_BOUNDING_BOXES = True
SHOW_DISTANCES = True
SAVE_ANNOTATED_IMAGE = True
OUTPUT_DIRECTORY = "path/to/save/annotated/images"
